package ru.t1.godyna.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "change-user-password";

    @NotNull
    private final String DESCRIPTION = "change password of current user.";

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-complete-by-index";

    @NotNull
    private final String DESCRIPTION = "Complete task by index.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(getUserId(), index, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

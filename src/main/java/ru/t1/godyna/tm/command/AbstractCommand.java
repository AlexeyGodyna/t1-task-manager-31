package ru.t1.godyna.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.model.ICommand;
import ru.t1.godyna.tm.api.service.IAuthService;
import ru.t1.godyna.tm.api.service.IServiceLocator;
import ru.t1.godyna.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    @Nullable
    public abstract String getName();

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @Nullable
    public String getUserId() {
        return getAuthService().getUserId();
    }

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    public abstract String getDescription();

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
